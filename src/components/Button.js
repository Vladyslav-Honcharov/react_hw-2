import React, { Component } from "react";
import "./Button.scss";
export class Button extends Component {
  constructor(props) {
    super(props);
    this.backgroundColor = props.backgroundColor;
    this.text = props.text;
    this.onClick = props.onClick;
  }

  render() {
    return (
      <button
        style={{ background: this.backgroundColor }}
        className="btn btn-outline-success btn-add"
        text={"Add to card"}
        onClick={this.props.onClick}
      >
        {this.text}
      </button>
    );
  }
}

export default Button;
