import React, { Component } from "react";
import "./Card.scss";
import * as bootstrap from "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import Button from "./Button";
import Modal from "./Modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { faStar as farStar } from "@fortawesome/free-regular-svg-icons";

export default class Card extends Component {
  constructor(props) {
    super(props);
    this.name = props.name;
    this.price = props.price;
    this.url = props.url;
    this.color = props.color;
    const storageKey = `isFavorite_${this.name}_${this.price}`;

    const isFavorite = JSON.parse(localStorage.getItem(storageKey)) || false;

    this.state = {
      isModalOpen: false,
      isFavorite: isFavorite,
      storageKey: storageKey,
    };
  }

  toggleFavorite = (e) => {
    e.preventDefault();
    const { isFavorite, storageKey } = this.state;
    const newFavorite = !isFavorite;
    this.setState({ isFavorite: newFavorite });
    const { name, price, url } = this.props;
    if (newFavorite) {
      this.props.addToFavorites({ name, price, url });
    } else {
      this.props.handleRemoveFromFavorites({ name, price, url });
    }
    localStorage.setItem(storageKey, JSON.stringify(newFavorite));
  };

  handleClick = () => {
    this.setState({ isModalOpen: true });
  };

  handleClose = () => {
    this.setState({ isModalOpen: false });
  };

  handleModalClick = (e) => {
    if (e.target.classList.contains("modal")) {
      this.setState({ isModalOpen: false });
    }
    return;
  };

  render() {
    const { isModalOpen, isFavorite } = this.state;
    const { name, price, url, color } = this.props;

    return (
      <div className="card" style={{ width: "18rem" }}>
        <div className="favorite-icon">
          <a href="" onClick={this.toggleFavorite}>
            <FontAwesomeIcon
              icon={isFavorite ? faStar : farStar}
              className={isFavorite ? "star-icon selected" : "star-icon"}
            />
          </a>
        </div>
        <img src={url} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{color}</p>
        </div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">{price} $</li>
        </ul>
        <Button text={"Add to card"} onClick={this.handleClick} />
        {isModalOpen && (
          <Modal
            header="Do you want to add this wine to your cart?"
            closeButton={true}
            action="AddCard"
            onClose={this.handleClose}
            btnOne="Ok"
            btnSecond="Cancel"
            updateCartCount={this.props.updateCartCount}
            onClickOutside={this.handleModalClick}
            onClick={this.addToCart}
            addToCart={this.props.addToCart}
            name={name}
            price={price}
            url={url}
          />
        )}
      </div>
    );
  }
}
