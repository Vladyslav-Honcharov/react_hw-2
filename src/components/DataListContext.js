import React, { createContext, Component } from "react";
import axios from "axios";

// Создаем контекст
export const DataListContext = createContext();

export default class DataListProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataList: [],
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {
    try {
      const response = await axios.get("data.json");
      this.setState({ dataList: response.data });
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  render() {
    const { dataList } = this.state;
    const { children } = this.props;

    // Предоставляем состояние и методы через контекст
    return (
      <DataListContext.Provider value={{ dataList }}>
        {children}
      </DataListContext.Provider>
    );
  }
}
