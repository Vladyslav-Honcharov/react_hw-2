import React, { Component } from "react";
import "./Favourites.scss";
import PropTypes from "prop-types";
export class Favorites extends Component {
  constructor(props) {
    super(props);

    this.favoritesList = props.favoritesList;

    this.state = {
      favoritesOpen: false,
    };
  }
  renderFavouritesItems() {
    return this.props.favoritesList.map((item, index) => (
      <div className="favourites m-4" key={index}>
        <div className="favourites-text">
          {" "}
          <div>{item.name}</div>
          <div>{item.price} $</div>
        </div>

        <img className="favourites-img" src={item.url} alt="" />
        {/* <div
          className="favourites-close"
          onClick={() => {
            this.props.handleRemoveFromFavorites(index);
          }}
        >
          ♥
        </div> */}
      </div>
    ));
  }
  render() {
    return (
      <>
        <div
          onClick={this.handleClick}
          className="header-favorites position-relative"
          data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasScrollingFavourites"
          aria-controls="offcanvasLeft"
        >
          <img
            src="https://cdn-icons-png.flaticon.com/512/865/865914.png?w=1480&t=st=1686210082~exp=1686210682~hmac=d037336c91c97beb99564d93fb89daadc2830a0b087c4960100b1f00ad08090c"
            alt=""
          />
          <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-secondary">
            {this.props.favoritesList.length}
          </span>
        </div>

        <div
          class="offcanvas offcanvas-start"
          tabindex="-1"
          data-bs-scroll="true"
          data-bs-backdrop="false"
          id="offcanvasScrollingFavourites"
          aria-labelledby="offcanvasRight"
        >
          <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="offcanvasRightLabel">
              Your favourites wine
            </h5>
            <button
              type="button"
              class="btn-close"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></button>
          </div>

          <div class="offcanvas-body favourites-body">
            {this.renderFavouritesItems()}
          </div>
        </div>
      </>
    );
  }
}
Favorites.propTypes = {
  favoritesList: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      url: PropTypes.string.isRequired,
    })
  ).isRequired,
  handleRemoveFromFavorites: PropTypes.func.isRequired,
};
Favorites.defaultProps = {
  favoritesList: PropTypes.arrayOf(
    PropTypes.shape({
      price: 10,
      url: "No foto",
    })
  ).isRequired,
  handleRemoveFromFavorites: PropTypes.func.isRequired,
};

export default Favorites;
