import PropTypes from "prop-types";
import React, { Component } from "react";
import Cart from "./Cart";
import Favorites from "./Favorites";
import "./Header.scss";
export class Header extends Component {
  render() {
    return (
      <div>
        <nav className="navbar ">
          <div className="container-fluid">
            <a className="navbar-brand" href="#">
              CoolWine
            </a>

            <div className="header-icons">
              {" "}
              <Cart
                cartList={this.props.cartList}
                handleRemoveFromCart={this.props.handleRemoveFromCart}
              />
              <Favorites
                favoritesList={this.props.favoritesList}
                handleRemoveFromFavorites={this.props.handleRemoveFromFavorites}
              />
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default Header;
